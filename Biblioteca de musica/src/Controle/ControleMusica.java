/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controle;

import Modelo.Musica;
import java.util.ArrayList;

/**
 *
 * @author Renan
 */
public class ControleMusica {

    private ArrayList<Musica> listaMusicas;
    
    public ControleMusica() {
        listaMusicas = new ArrayList<Musica>(); 
    }

    public ArrayList<Musica> getListaMusicas() {
        return listaMusicas;
    }

    public void setListaMusicas(ArrayList<Musica> listaMusicas) {
        this.listaMusicas = listaMusicas;
    }
    
    public String AdicionarMusica(Musica umaMusica){
        listaMusicas.add(umaMusica);
        return "Musica adicionada com sucesso!";
    }
    public Musica pesquisarMusica(String nomeMusica){
        for (Musica umaMusica: listaMusicas){
            if (umaMusica.getCancaoMp3().equalsIgnoreCase(nomeMusica))
                return umaMusica;
            }
            return null;
        
    }
}
