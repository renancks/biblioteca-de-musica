/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;



/**
 *
 * @author Renan
 */
public class Musica {
    
    private String cancaoMp3;

    public static final String PROP_CANCAOMP3 = "cancaoMp3";

    public String getCancaoMp3() {
        return cancaoMp3;
    }

    public void setCancaoMp3(String cancaoMp3) {
        String oldCancaoMp3 = this.cancaoMp3;
        this.cancaoMp3 = cancaoMp3;
        propertyChangeSupport.firePropertyChange(PROP_CANCAOMP3, oldCancaoMp3, cancaoMp3);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    private String nomeArtista;

    public static final String PROP_NOMEARTISTA = "nomeArtista";

    public String getNomeArtista() {
        return nomeArtista;
    }

    public void setNomeArtista(String nomeArtista) {
        String oldNomeArtista = this.nomeArtista;
        this.nomeArtista = nomeArtista;
        propertyChangeSupport.firePropertyChange(PROP_NOMEARTISTA, oldNomeArtista, nomeArtista);
    }

    private String album;

    public static final String PROP_ALBUM = "album";

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        String oldAlbum = this.album;
        this.album = album;
        propertyChangeSupport.firePropertyChange(PROP_ALBUM, oldAlbum, album);
    }

    
   /* private String nomeArtista;
    private String cancaoMp3;
    private String album;

    public Musica(){
    
    }
    public Musica(String nomeArtista, String cancaoMp3, String album) {
        this.nomeArtista = nomeArtista;
        this.cancaoMp3 = cancaoMp3;
        this.album = album;
    }
    
    

    public String getNomeArtista() {
        return nomeArtista;
    }

    public void setNomeArtista(String nomeArtista) {
        this.nomeArtista = nomeArtista;
    }

    public String getCancaoMp3() {
        return cancaoMp3;
    }

    public void setCancaoMp3(String cancaoMp3) {
        this.cancaoMp3 = cancaoMp3;
    }

  
   
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
    
    */
}
