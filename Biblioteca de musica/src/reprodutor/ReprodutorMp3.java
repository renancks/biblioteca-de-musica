

package reprodutor;

import java.io.IOException;
import java.net.URL;
import javax.media.Manager;
import javax.media.Player;
import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Renan
 */
public class ReprodutorMp3 {
    private Player mediaPlayer;
    private Timer tempo;
    private TimerTask tarefa;
    private int vel = 1000;
    private int quadro;
    
    
    public void play(URL url){
    
        try{
            mediaPlayer = Manager.createRealizedPlayer(url);
        
        }catch(Exception ex) {
        
        }
        
    }
    
    public void parar(){
        mediaPlayer.stop();
        
    }
    
    public void tocar(){
    
        mediaPlayer.start();
    }
    
    public void mover(final JSlider s){
        tempo = new Timer();
        tarefa = new TimerTask(){
            public void run(){
                quadro = (int) Math.round((mediaPlayer.getMediaTime().getSeconds() *100) / mediaPlayer.getDuration().getSeconds());
                if(mediaPlayer.getMediaTime().getSeconds() == mediaPlayer.getDuration().getSeconds()){
                    quadro = 100;
                    parar();
                }else{
                    s.setValue(quadro);
                }
                
            }
        };
        tempo.schedule(tarefa, 0, vel);
    }
    
    public void paraBarra(final JSlider s){
        tempo.cancel();
        tarefa.cancel();
    }
    public float getVolume(){
        return mediaPlayer.getGainControl().getLevel();
    }
    
    public void setVolume(float volume){
        mediaPlayer.getGainControl().setLevel(volume);
    }
    
}
